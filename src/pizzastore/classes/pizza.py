class Pizza:

    def __init__(self, idx, name, price, ingredients):
        self.idx = idx
        self.name = name
        self.price = price
        self.ingredients = ingredients

    def print_receipt(self):
        return f'{self.name}'

    def __str__(self):
        return f'Name: {self.name}, Price: {self.price}' \
               f'\nIngredients: {self.ingredients}'

