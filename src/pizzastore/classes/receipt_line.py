from pizzastore.classes.pizza import Pizza


class ReceiptLine:

    def __init__(self, pizza: Pizza, num: int):
        self.pizza = pizza
        self.num = num

    def change_num(self, number):

        if number > 0:
            self.num += number
        elif abs(number) < self.num and number < 0:
            self.num += number

    def __str__(self):
        return f'{self.pizza.print_receipt():<18} {self.num} {self.pizza.price * self.num:>15}\n'


