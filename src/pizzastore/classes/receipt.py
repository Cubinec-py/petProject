from datetime import datetime
from pizzastore.classes.receipt_line import ReceiptLine
from pizzastore.classes.personal import Personal
from pizzastore.classes.tables import Table


class Receipt:
    n_receipt = 1

    def __init__(self, cashier: Personal, table: Table):
        self.lines = []
        self.data1 = datetime.now().strftime('%Y-%m-%d')
        self.data2 = datetime.now().strftime('%H:%M:%S')
        self.num = Receipt.n_receipt
        self.cashier = cashier
        self.table = table
        Receipt.n_receipt += 1

    def add_line(self, line: ReceiptLine):
        self.lines.append(line)

    def __str__(self):
        dec = '-' * 39
        total = []
        for i in self.lines:
            total.append(i.num * i.pizza.price)
        return f'Check №{self.num:<14} Table {self.table}' \
               f'\nOpen {self.data1:<16} Cashier {self.cashier}' \
               f'\n{self.data2:>15}' \
               f'\n{"Pizza":<15} {"Amount":<15} Price ' \
               f'\n{dec}' \
               f'\n{"".join([str(item) for item in self.lines])}' \
               f'\n{dec}' \
               f'\nTotal {sum(total):>30}' \
               f'\nService 10% {int(sum(total) * 0.1):>24}' \
               f'\nTotal to pay{sum(total)+int(sum(total) * 0.1):>24}\n'