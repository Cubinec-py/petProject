class MyException(Exception):

    def __init__(self, price, message='We do not have pizza in this range of price:'):
        self.price = price
        self.message = message

    def __str__(self):
        return f'{self.message} {self.price}'