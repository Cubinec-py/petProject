from pizzastore.classes.personal import Personal
from pizzastore.classes.pizza import Pizza
from pizzastore.classes.receipt import Receipt
from pizzastore.classes.tables import Table

from pizzastore.const.cashiers import cashiers
from pizzastore.const.table import table

import random
import csv


read_items = open('src/pizzastore/const/pizza_list.txt', 'r')
pizza_items = csv.reader(read_items, delimiter='.', quotechar='|', quoting=csv.QUOTE_MINIMAL)


def decorator_pizza(func):
    def inner(self, pizza):
        dec = '*' * 90
        print(dec)
        func(self, pizza)
        print('-' * 90, f'\nNumber of pizzas: {len(pizza)}')
        print(dec)
    return inner


def decorator_cost(func):
    def inner(self, pizza):
        dec = '*' * 41
        print(dec)
        func(self, pizza)
        print(dec)
    return inner


class SingletonMeta(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):

        if cls not in cls._instances:
            instance = super().__call__(*args, **kwargs)
            cls._instances[cls] = instance
        return cls._instances[cls]


class PizzaStore(metaclass=SingletonMeta):

    def __init__(self):
        self.pizzas = [Pizza(int(i[0]), i[1], int(i[2]), i[3]) for i in pizza_items]
        self.receipts = []
        self.personal = [Personal(*i) for i in cashiers]
        self.table = [Table(*i) for i in table]

    def print_receipts(self):
        for item in self.receipts:
            print(item)

    def add_receipts(self, receipt_item: Receipt):
        self.receipts.append(receipt_item)

    def get_cashier(self):
        return random.choice(self.personal)

    def get_table(self):
        return random.choice(self.table)

    @staticmethod
    def greeting():
        print('Welcome to our Pizza Store! ',
              'Exit: 0 ',
              'Menu: 1 ',
              'Add check with pizzas: 2 ',
              'View existing checks: 3',
              'Edit check: 4',
              'Pizzas less and higher 185: 5',
              'Pizza lees or high using user input: 6', sep='\n')

    @decorator_pizza
    def lst_menu(self, pizzas):
        print(f'{"Our menu:":<100}')
        for elem in pizzas:
            print('-' * 90, f'\n№{(elem.idx + 1):<5} {"Name:"} {elem.name:<60} Price: {elem.price}'
                  f'\n{"Ingredients:":>19} {elem.ingredients}')

    @decorator_cost
    def less_higher(self, pizzas):
        print('Here our pizzas lower 185:')

        for i in pizzas:
            if i.price < 185:
                print(f'№{(i.idx + 1):<2} Name: {i.name:<19} Price: {i.price}')

        print('\nHere our pizzas higher 185:')

        for i in pizzas:
            if i.price > 185:
                print(f'№{(i.idx + 1):<2} Name: {i.name:<16} \t  Price: {i.price}')