from pizzastore.classes.pizza_store import PizzaStore
from pizzastore.classes.receipt import Receipt
from pizzastore.classes.receipt_line import ReceiptLine
from pizzastore.classes.exception import MyException


import random


def run_pizza_store():
    while True:
        store.greeting()
        menu = input("Choose your number: ")

        match menu:

            case '0':
                print('Exit')
                break

            case '1':
                store.lst_menu(store.pizzas)

            case '2':
                check = Receipt(store.get_cashier(), store.get_table())
                random_amount = random.sample(store.pizzas, random.randint(1, 5))
                for pizzas_items in random_amount:
                    check.add_line(ReceiptLine(pizzas_items, random.randint(1, 5)))
                store.add_receipts(check)
                store.print_receipts()

            case '3':
                store.print_receipts()

            case '4':
                while len(store.receipts) >= 0:

                    if len(store.receipts) == 0:
                        print('There are currently no checks in the system')
                        break

                    while len(store.receipts) > 0:
                        store.print_receipts()

                        while True:
                            try:
                                choice = int(input('Choose check, which you want to change: '))
                                check = store.receipts[choice - 1]
                                print(check)
                                break
                            except IndexError:
                                print('Please write the right number of check!')
                                continue

                        choice2 = int(input('You want to change pizza?(1 - Yes, 0 - No): '))
                        while choice2 == 1:

                            while True:
                                try:
                                    choice2 = int(input('Choose number of pizza, which you want to change: '))
                                    line = check.lines[choice2 - 1]
                                    break
                                except IndexError:
                                    print('Please, write the right line of pizza!')
                                    continue

                            while True:
                                try:
                                    choice3 = int(input('How much you want add(1 - 10) or delete(-1 - -10)?: '))
                                    line.change_num(choice3)
                                    break
                                except IndexError:
                                    print('Please, write the right line of pizza!')
                                    continue

                        choice4 = int(input('Want to add new pizza?(1 - Yes, 0 - No): '))

                        if choice4 == 0:
                            print('Check looks like this now')
                            print(check)

                        if choice4 == 1:
                            store.lst_menu(store.pizzas)

                            while True:
                                choice5 = int(input('Choose number of pizza: '))
                                if choice5 in range(1, 10):
                                    break
                                else:
                                    print('Write the right number of pizza!')
                                    continue
                            while True:
                                choice6 = int(input('Choose amount of pizza 1 - 10: '))
                                if choice6 in range(1, 10):
                                    break
                                else:
                                    print('Write the right amount of pizza!')
                                    continue

                            for i in store.pizzas:
                                if i.idx == choice5 - 1:
                                    check.add_line(ReceiptLine(i, choice6))
                                    print('Check looks like this now')
                                    print(check)
                        break
                    break

            case '5':
                store.less_higher(store.pizzas)

            case '6':
                price_all = []

                for i in store.pizzas:
                    price_all.append(i.price)

                price_min = price_all[0]
                price_max = price_all[0]

                for i in price_all:
                    if i < price_min:
                        price_min = i

                for i in price_all:
                    if i > price_max:
                        price_max = i

                choice = int(input('You want to see pizza lower or higher of your needed price(1 - lower, 2 - higher): '))

                if choice == 1:

                    while True:
                        choice2 = int(input('Enter your price and you can see what we have: '))
                        if choice2 < price_min or choice2 > price_max:
                            raise MyException(choice2)
                        for i in store.pizzas:
                            if i.price <= choice2:
                                print('-'*90, i, sep='\n')
                        break

                if choice == 2:

                    while True:
                        choice2 = int(input('Enter your price and you can see what we have: '))
                        if choice2 < price_min or choice2 > price_max:
                            raise MyException(choice2)
                        for i in store.pizzas:
                            if i.price >= choice2:
                                print('-'*90, i, sep='\n')
                        break

            case _:
                print('Wrong number!')


if __name__ == '__main__':
    store = PizzaStore()
    run_pizza_store()